# EmiFrontend

This test project for Rabobank was generated with [Angular CLI](https://github.com/angular/angular-cli) version 16.1.0.

## Prerequisites

* Node.js
* npm

# Run locally

> ** IMPORTANT ** 
To run the project the backend service is requiered as well. Please refer to [Backend project docs](https://gitlab.com/rabo-emi/frontend/-/blob/main/README.md).


1. [Download](https://gitlab.com/rabo-emi/frontend/-/archive/main/frontend-main.zip) or clone the [repository](https://gitlab.com/rabo-emi/frontend.git) to your local machine:
```bash
$ git clone https://gitlab.com/rabo-emi/frontend.git
```

2. Run `npm install` inside the downloaded/cloned folder:
```bash
$ npm install
```

3. Start the dev server by running the command below. Navigate to `http://localhost:4200/`.
```bash
$ npm start
```

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory.