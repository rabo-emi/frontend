import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormBuilder } from '@angular/forms';
import { isPositiveNumberValidator } from './custom-validators';
import { HttpClient } from '@angular/common/http';
import { RequestFormService } from '../request-form.service';

@Component({
  selector: 'app-request-form',
  templateUrl: './request-form.component.html',
  styleUrls: ['./request-form.component.css']
})
export class RequestFormComponent implements OnInit{

  requestForm!: FormGroup;
  submitted = false;

  constructor(
    private fb: FormBuilder,
    private http: HttpClient,
    private requestFormService: RequestFormService
  ) { }

  ngOnInit(): void {
    this.requestForm = this.fb.group({
      loanValue: [10000, [Validators.required, isPositiveNumberValidator()]],
      interestRate: [5, [Validators.required, isPositiveNumberValidator(), Validators.max(100)]],
      loanTerm: [30, [Validators.required, isPositiveNumberValidator(), Validators.max(30)]],
    });    
  }

  get requestFormControl() {
    return this.requestForm.controls;
  }

  onSubmit() {
    if (this.requestForm.valid) {
      this.submitted = true;
      const body = {
        loanValue: this.requestForm.controls['loanValue'].value,
        interestRate: this.requestForm.controls['interestRate'].value,
        loanTerm: this.requestForm.controls['loanTerm'].value,
      };

      const emi = this.requestFormService.getEmi(body.loanValue, body.interestRate, body.loanTerm).subscribe((response: number) => {
        console.log(response)
        alert('Your Equated Monthly Installment for requested parameters would be ' + response);
      });

    } else {
      alert('Invalid request');
      console.error(this.requestFormControl);
    }
  }
}
