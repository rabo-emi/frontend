
import {AbstractControl, ValidationErrors, ValidatorFn} from '@angular/forms';

export function isPositiveNumberValidator(): ValidatorFn {
    return (control: AbstractControl) : ValidationErrors | null => {

        const value = control.value!;
        
        return (value <= 0) ? {nonPositiveNumber: true}: null;
    }
}