import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class RequestFormService {

  constructor(
    private http: HttpClient) { }

  public getEmi(loanValue: number, interestRate: number, loanTerm: number) {
    const url = 'http://127.0.0.1:8080/calculate';
    let queryParams = new HttpParams();
    queryParams = queryParams.append('loanValue', loanValue);
    queryParams = queryParams.append('interestRate', interestRate);
    queryParams = queryParams.append('loanTerm', loanTerm);

    return this.http.get<number>(url, { params: queryParams });
  }
}
